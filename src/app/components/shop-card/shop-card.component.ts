import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { Shop } from "../../models/shop.interface";

@Component({
  selector: "app-shop-card",
  templateUrl: "./shop-card.component.html",
  styleUrls: ["./shop-card.component.scss"],
})
export class ShopCardComponent implements OnInit {
  @Input() shop: Shop;
  @Output() onSelectShop = new EventEmitter<Shop>();

  constructor() {}

  ngOnInit(): void {}

  selectShop() {
    this.onSelectShop.emit(this.shop);
  }
}

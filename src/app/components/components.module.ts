import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ShopCardComponent } from "./shop-card/shop-card.component";
import { TabsModule } from "ngx-bootstrap/tabs";
import { RouterModule } from "@angular/router";
import { GameComponent } from "./game/game.component";
import { ModalModule } from "ngx-bootstrap/modal";
import { CarouselModule } from "ngx-bootstrap/carousel";
import { ProgressbarModule } from "ngx-bootstrap/progressbar";
import { JwBootstrapSwitchNg2Module } from "jw-bootstrap-switch-ng2";

@NgModule({
  imports: [
    CommonModule,
    RouterModule,

    TabsModule.forRoot(),
    ModalModule.forRoot(),
    CarouselModule.forRoot(),
    ProgressbarModule.forRoot(),
    JwBootstrapSwitchNg2Module,
  ],
  declarations: [ShopCardComponent, GameComponent],
  exports: [ShopCardComponent, GameComponent],
  providers: [],
})
export class ComponentsModule {}

import { Component, Input, OnInit } from "@angular/core";
import { Game } from "../../models/game.interface";
import { Store } from "@ngrx/store";
import { gameDetailSelectors } from "../../pages/shop-detail/store/games-detail/game-detail.selectors";
import { gameDetailActions } from "../../pages/shop-detail/store/games-detail/games-detail.actions";

@Component({
  selector: "app-game",
  templateUrl: "./game.component.html",
  styleUrls: ["./game.component.scss"],
})
export class GameComponent implements OnInit {
  @Input() game: Game;

  gameDetail$ = this.store.select(gameDetailSelectors.selectGame);

  constructor(private store: Store) {}

  ngOnInit(): void {}

  getInfo() {
    this.store.dispatch(
      gameDetailActions.getGameDetail({ id: this.game.id.toString() })
    );
  }

  calculateRating(rating: number) {
    return ((rating * 100) / 5).toFixed(2);
  }
}

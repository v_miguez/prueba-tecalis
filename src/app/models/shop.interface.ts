import { Game } from "./game.interface";

export interface ShopDTO {
  count: number;
  next?: any;
  previous?: any;
  results: Shop[];
}

export interface Shop {
  id: number;
  name: string;
  domain: string;
  slug: string;
  games_count: number;
  image_background: string;
  games: Game[];
}

export interface ShopDetail {
  id: number;
  name: string;
  domain: string;
  slug: string;
  games_count: number;
  image_background: string;
  description: string;
}

import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { HomeComponent } from "./home.component";
import { ComponentsModule } from "../../components/components.module";
import { StoreModule } from "@ngrx/store";
import { shopsReducer } from "./store/shops.reducer";
import { RouterModule } from "@angular/router";
import { EffectsModule } from "@ngrx/effects";
import { ShopsEffects } from "./store/shops.effects";

const routes = [
  {
    path: "",
    component: HomeComponent,
    pathMatch: "full",
  },
  {
    path: "**",
    redirectTo: "",
  },
];

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    RouterModule.forChild(routes),

    EffectsModule.forFeature([ShopsEffects]),
  ],
  declarations: [HomeComponent],
  exports: [],
  providers: [],
})
export class HomeModule {}

import { createReducer, on } from "@ngrx/store";
import { Shop } from "../../../models/shop.interface";
import { shopsActions } from "./shops.actions";

export interface ShopsState {
  shops: Shop[];
  selectedShop: Shop;
  loading: boolean;
  error: boolean;
}

export const initialState: ShopsState = {
  shops: [],
  selectedShop: null,
  loading: false,
  error: false,
};

export const shopsReducer = createReducer(
  initialState,
  on(shopsActions.getShops, (state) => ({
    ...state,
    loading: true,
  })),
  on(shopsActions.getShopsSuccess, (state, { data }) => ({
    ...state,
    shops: data,
    loading: false,
    error: false,
  })),
  on(shopsActions.getShopsFailure, (state) => ({
    ...state,
    loading: false,
    error: true,
  })),
  on(shopsActions.onSelectShop, (state, { shop }) => ({
    ...state,
    selectedShop: shop,
  }))
);

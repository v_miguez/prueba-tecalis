import { ShopsState } from "./shops.reducer";
import { createSelector } from "@ngrx/store";
import { createFeatureSelector } from "@ngrx/store";

export const selectFeature = createFeatureSelector<ShopsState>("shops");

const selectShops = createSelector(selectFeature, (state) => state.shops);

const selectSelectedShop = createSelector(
  selectFeature,
  (state) => state.selectedShop
);

const shopsIsLoading = createSelector(selectFeature, (state) => state.loading);

const shopsError = createSelector(selectFeature, (state) => state.error);

export const shopsSelectors = {
  selectShops,
  selectSelectedShop,
  shopsIsLoading,
  shopsError,
};

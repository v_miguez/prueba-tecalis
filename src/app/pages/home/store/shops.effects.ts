import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { shopsActions } from "./shops.actions";
import { map, catchError, switchMap } from "rxjs/operators";
import { EMPTY } from "rxjs";

import { ShopsService } from "../services/shops.service";

@Injectable()
export class ShopsEffects {
  $enterPage = createEffect(() =>
    this.actions$.pipe(
      ofType(shopsActions.enterPage),
      map(() => shopsActions.getShops())
    )
  );

  loadShops$ = createEffect(() =>
    this.actions$.pipe(
      ofType(shopsActions.getShops),
      switchMap(() =>
        this.shopsService.getShops().pipe(
          map((items) => shopsActions.getShopsSuccess({ data: items.results })),
          catchError(() => EMPTY)
        )
      )
    )
  );

  constructor(private actions$: Actions, private shopsService: ShopsService) {}
}

import { createAction, props } from "@ngrx/store";
import { Shop } from "../../../models/shop.interface";

const enterPage = createAction("[Shops Page] Enter Page");

const getShops = createAction("[Shops API] Shops api request");

const getShopsSuccess = createAction(
  "[Shops API] Shops api request success",
  props<{ data: Shop[] }>()
);

const getShopsFailure = createAction(
  "[Shops API] Shops api request failure"
);

const onSelectShop = createAction(
  "[Shops Page] Select Shop",
  props<{ shop: Shop }>()
);

export const shopsActions = {
  enterPage,
  getShops,
  getShopsSuccess,
  getShopsFailure,
  onSelectShop,
};

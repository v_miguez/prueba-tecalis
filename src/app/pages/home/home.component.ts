import { Component, OnInit } from "@angular/core";
import { Shop } from "../../models/shop.interface";
import { Store } from "@ngrx/store";
import { shopsActions } from "./store/shops.actions";
import { shopsSelectors } from "./store/shops.selectors";
import { Router } from "@angular/router";
@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit {
  shops$ = this.store.select(shopsSelectors.selectShops);
  loading$ = this.store.select(shopsSelectors.shopsIsLoading);

  constructor(private store: Store, private router: Router) {}

  ngOnInit(): void {
    this.store.dispatch(shopsActions.enterPage());
  }

  onSelectShop(shop: Shop) {
    this.store.dispatch(shopsActions.onSelectShop({ shop }));
    this.router.navigate(["/shop", shop.id]);
  }
}

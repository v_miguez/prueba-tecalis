import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ShopDTO } from "../../../models/shop.interface";
import { environment } from "../../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class ShopsService {
  constructor(private http: HttpClient) {}

  getShops(): Observable<ShopDTO> {
    return this.http.get<ShopDTO>(
      `${environment.apiUrl}/stores?key=${environment.apiKey}`
    );
  }
}

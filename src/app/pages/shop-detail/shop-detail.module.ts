import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ComponentsModule } from "../../components/components.module";
import { StoreModule } from "@ngrx/store";
import { RouterModule } from "@angular/router";
import { EffectsModule } from "@ngrx/effects";
import { ShopsEffects } from "../home/store/shops.effects";
import { ShopDetailComponent } from "./shop-detail.component";
import { shopDetailReducer } from "./store/shop-detail/shops-detail.reducer";
import { ShopDetailEffects } from "./store/shop-detail/shops-detail.effects";
import { ModalModule } from "ngx-bootstrap/modal";
import { gameDetailReducer } from "./store/games-detail/games-detail.reducer";
import { GameDetailEffects } from "./store/games-detail/games-detail.effects";

const routes = [
  {
    path: "",
    component: ShopDetailComponent,
    pathMatch: "full",
  },
  {
    path: "**",
    redirectTo: "",
  },
];

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    RouterModule.forChild(routes),
    StoreModule.forFeature("shopDetail", shopDetailReducer),
    StoreModule.forFeature("gameDetail", gameDetailReducer),
    EffectsModule.forFeature([ShopDetailEffects, GameDetailEffects]),
    ModalModule,
  ],
  declarations: [ShopDetailComponent],
  exports: [ShopDetailComponent],
  providers: [],
})
export class ShopDetailModule {}

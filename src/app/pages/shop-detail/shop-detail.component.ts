import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { gamesSelectors } from "../../store/games/games.selectors";
import { shopDetailActions } from "./store/shop-detail/shop-detail.actions";
import { shopDetailSelectors } from "./store/shop-detail/shops-detail.selectors";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-shop-detail",
  templateUrl: "./shop-detail.component.html",
  styleUrls: ["./shop-detail.component.scss"],
})
export class ShopDetailComponent implements OnInit {
  shopDetail$ = this.store.select(shopDetailSelectors.selectShop);
  expanded = true;

  games$ = this.store.select(gamesSelectors.selectGames);

  constructor(private store: Store, private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.store.dispatch(shopDetailActions.enterPage({ id: params.id }));
    });
  }
}

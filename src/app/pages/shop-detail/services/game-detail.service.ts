import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "../../../../environments/environment";
import { GameDetail } from "../../../models/game.interface";

@Injectable({
  providedIn: "root",
})
export class GameDetailService {
  constructor(private http: HttpClient) {}

  getGameDetail(id: string): Observable<GameDetail> {
    return this.http.get<GameDetail>(
      `${environment.apiUrl}/games/${id}?key=${environment.apiKey}`
    );
  }
}

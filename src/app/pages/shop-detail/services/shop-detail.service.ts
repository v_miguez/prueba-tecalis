import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ShopDetail } from "../../../models/shop.interface";
import { environment } from "../../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class ShopDetailService {
  constructor(private http: HttpClient) {}

  getShopDetail(id: string): Observable<ShopDetail> {
    return this.http.get<ShopDetail>(
      `${environment.apiUrl}/stores/${id}?key=${environment.apiKey}`
    );
  }
}

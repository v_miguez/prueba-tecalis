import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { map, switchMap, catchError } from "rxjs/operators";
import { gameDetailActions } from "./games-detail.actions";
import { of } from "rxjs";
import { GameDetailService } from "../../services/game-detail.service";

@Injectable()
export class GameDetailEffects {
  enterPage$ = createEffect(() =>
    this.actions$.pipe(
      ofType(gameDetailActions.enterPage),
      switchMap((id) => {
        return of(gameDetailActions.getGameDetail(id));
      })
    )
  );

  loadGames$ = createEffect(() =>
    this.actions$.pipe(
      ofType(gameDetailActions.getGameDetail),
      switchMap((actionParams) =>
        this.gameDetailService.getGameDetail(actionParams.id).pipe(
          map((items) =>
            gameDetailActions.getGameDetailSuccess({ data: items })
          ),
          catchError(() => of(gameDetailActions.getGameDetailFailure()))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private gameDetailService: GameDetailService
  ) {}
}

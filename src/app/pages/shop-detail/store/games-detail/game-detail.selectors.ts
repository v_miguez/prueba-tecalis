import { GameDetailState } from "./games-detail.reducer";
import { createSelector } from "@ngrx/store";
import { createFeatureSelector } from "@ngrx/store";

export const selectFeature =
  createFeatureSelector<GameDetailState>("gameDetail");

const selectGame = createSelector(selectFeature, (state) => state.game);

export const gameDetailSelectors = {
  selectGame,
};

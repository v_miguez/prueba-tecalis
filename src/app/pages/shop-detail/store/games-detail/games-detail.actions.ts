import { createAction, props } from "@ngrx/store";
import { GameDetail } from "../../../../models/game.interface";

const enterPage = createAction(
  "[Games Page] Enter Page",
  props<{ id: string }>()
);

const getGameDetail = createAction(
  "[Games API] Games api request",
  props<{ id: string }>()
);

const getGameDetailSuccess = createAction(
  "[Games API] Games api request success",
  props<{ data: GameDetail }>()
);

const getGameDetailFailure = createAction(
  "[Games API] Games api request failure",
);

export const gameDetailActions = {
  enterPage,
  getGameDetail,
  getGameDetailSuccess,
  getGameDetailFailure,
};

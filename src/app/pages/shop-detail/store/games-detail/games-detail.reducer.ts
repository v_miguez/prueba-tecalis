import { createReducer, on } from "@ngrx/store";
import { gameDetailActions } from "./games-detail.actions";
import { Game } from "../../../../models/game.interface";

export interface GameDetailState {
  game: Game;
  loading: boolean;
  error: boolean;
}

export const initialState: GameDetailState = {
  game: null,
  loading: false,
  error: false,
};

export const gameDetailReducer = createReducer(
  initialState,
  on(gameDetailActions.getGameDetail, (state) => ({
    ...state,
    loading: true,
  })),
  on(gameDetailActions.getGameDetailSuccess, (state, { data }) => ({
    ...state,
    game: data,
    loading: false,
    error: false,
  })),
  on(gameDetailActions.getGameDetailFailure, (state) => ({
    ...state,
    loading: false,
    error: true,
  }))
);

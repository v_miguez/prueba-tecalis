import { createAction, props } from "@ngrx/store";
import { ShopDetail } from "../../../../models/shop.interface";

const enterPage = createAction(
  "[Shop Detail Page] Enter Page",
  props<{ id: string }>()
);

const getShopDetail = createAction(
  "[Shops Detail API] Shop Detail api request",
  props<{ id: string }>()
);

const getShopDetailSuccess = createAction(
  "[Shops Detail API] Shop Detail api request success",
  props<{ data: ShopDetail }>()
);

const getShopDetailFailure = createAction(
  "[Shops Detail API] Shop Detail api request failure"
);

export const shopDetailActions = {
  getShopDetail,
  getShopDetailSuccess,
  getShopDetailFailure,
  enterPage,
};

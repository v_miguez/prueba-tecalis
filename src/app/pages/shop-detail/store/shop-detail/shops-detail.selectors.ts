import { createSelector, createFeatureSelector } from "@ngrx/store";
import { ShopDetailState } from "./shops-detail.reducer";

export const selectFeature =
  createFeatureSelector<ShopDetailState>("shopDetail");

const selectShop = createSelector(selectFeature, (state) => {
  return state.shop;
});

export const shopDetailSelectors = {
  selectShop,
};

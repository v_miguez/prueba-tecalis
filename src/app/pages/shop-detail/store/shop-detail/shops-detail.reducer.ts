import { createReducer, on } from "@ngrx/store";
import { ShopDetail } from "../../../../models/shop.interface";
import { shopDetailActions } from "./shop-detail.actions";

export interface ShopDetailState {
  shop: ShopDetail;
  loading: boolean;
  error: boolean;
}

export const initialState: ShopDetailState = {
  shop: null,
  loading: false,
  error: false,
};

export const shopDetailReducer = createReducer(
  initialState,
  on(shopDetailActions.enterPage, (state) => ({
    ...state,
    shop: null,
    loading: true,
  })),
  on(shopDetailActions.getShopDetail, (state) => ({
    ...state,
    loading: true,
  })),
  on(shopDetailActions.getShopDetailSuccess, (state, { data }) => ({
    ...state,
    shop: data,
    loading: false,
    error: false,
  })),
  on(shopDetailActions.getShopDetailFailure, (state) => ({
    ...state,
    loading: false,
    error: true,
  }))
);

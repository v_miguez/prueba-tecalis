import { Injectable } from "@angular/core";
import { Actions, concatLatestFrom, createEffect, ofType } from "@ngrx/effects";
import { map, catchError, switchMap, filter } from "rxjs/operators";
import { EMPTY } from "rxjs";
import { ShopDetailService } from "../../services/shop-detail.service";
import { shopDetailActions } from "./shop-detail.actions";
import { shopsSelectors } from "../../../home/store/shops.selectors";
import { Store } from "@ngrx/store";
import { gamesSelectors } from "../../../../store/games/games.selectors";
import { ShopsService } from "../../../home/services/shops.service";
import { ShopDTO } from "../../../../models/shop.interface";
import { gamesActions } from "../../../../store/games/games.actions";

@Injectable()
export class ShopDetailEffects {
  enterPage$ = createEffect(() =>
    this.actions$.pipe(
      ofType(shopDetailActions.enterPage),
      map((id) => shopDetailActions.getShopDetail(id))
    )
  );

  loadShops$ = createEffect(() =>
    this.actions$.pipe(
      ofType(shopDetailActions.getShopDetail),
      switchMap((actionParams) =>
        this.shopsDetailService.getShopDetail(actionParams.id).pipe(
          map((items) => {
            return shopDetailActions.getShopDetailSuccess({ data: items });
          }),
          catchError(() => EMPTY)
        )
      )
    )
  );

  getShopGames$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(shopDetailActions.enterPage),
        concatLatestFrom(() => [
          this.store.select(shopsSelectors.selectSelectedShop),
          this.store.select(gamesSelectors.selectGames),
        ]),
        filter(([actionParams, shop, games]) => {
          return games.length === 0 && !shop;
        }),
        switchMap(([actionParams]) => {
          return this.shopsService.getShops().pipe(
            map((shops: ShopDTO) => {
              const shopSelected = shops.results.find(
                (shop) => shop.id === Number(actionParams.id)
              );
              this.store.dispatch(
                gamesActions.setShopGames({ games: shopSelected.games })
              );
            }),
            catchError(() => EMPTY)
          );
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private shopsDetailService: ShopDetailService,
    private store: Store,
    private shopsService: ShopsService
  ) {}
}

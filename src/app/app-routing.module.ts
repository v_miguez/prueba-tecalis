import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  {
    path: "home",
    loadChildren: () =>
      import("./pages/home/home.module").then((m) => m.HomeModule),
  },
  {
    path: "shop/:id",
    loadChildren: () =>
      import("./pages/shop-detail/shop-detail.module").then(
        (m) => m.ShopDetailModule
      ),
  },

  {
    path: "examples",
    loadChildren: () =>
      import("./pages/examples/examples.module").then((m) => m.ExamplesModule),
  },
  { path: "", redirectTo: "home", pathMatch: "full" },
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, {
      useHash: true,
    }),
  ],
  exports: [],
})
export class AppRoutingModule {}

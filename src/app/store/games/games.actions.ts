import { createAction, props } from "@ngrx/store";

const setShopGames = createAction(
  "[Games] Set Games",
  props<{ games: any[] }>()
);

export const gamesActions = {
  setShopGames,
};

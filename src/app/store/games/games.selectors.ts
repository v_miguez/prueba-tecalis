import { GamesState } from "./games.reducer";
import { createSelector } from "@ngrx/store";
import { createFeatureSelector } from "@ngrx/store";

export const selectFeature = createFeatureSelector<GamesState>("games");

const selectGames = createSelector(selectFeature, (state) => state.games);

export const gamesSelectors = {
  selectGames,
};

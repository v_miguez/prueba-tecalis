import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { shopsActions } from "../../pages/home/store/shops.actions";
import { switchMap } from "rxjs/operators";
import { gamesActions } from "./games.actions";
import { of } from "rxjs";

@Injectable()
export class GamesEffects {
  loadGames$ = createEffect(() =>
    this.actions$.pipe(
      ofType(shopsActions.onSelectShop),
      switchMap(({ shop }) => {
        if (!shop) return of(gamesActions.setShopGames({ games: [] }));
        return of(gamesActions.setShopGames({ games: shop.games }));
      })
    )
  );

  constructor(private actions$: Actions) {}
}

import { createReducer, on } from "@ngrx/store";
import { gamesActions } from "./games.actions";

export interface GamesState {
  games: any[];
}

export const initialState: GamesState = {
  games: [],
};

export const gamesReducer = createReducer(
  initialState,
  on(gamesActions.setShopGames, (state, { games }) => ({
    ...state,
    games: games,
  }))
);

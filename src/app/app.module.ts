import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";

import { StoreModule } from "@ngrx/store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { EffectsModule } from "@ngrx/effects";
import { GamesEffects } from "./store/games/games.effects";
import { gamesReducer } from "./store/games/games.reducer";
import { RapidApiInterceptor } from "./interceptors/rapid-api.interceptor";
import { shopsReducer } from "./pages/home/store/shops.reducer";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    AppRoutingModule,
    StoreModule.forRoot({ games: gamesReducer }, {}),
    StoreModule.forFeature("shops", shopsReducer),
    EffectsModule.forRoot([GamesEffects]),
    StoreDevtoolsModule.instrument({}),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RapidApiInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

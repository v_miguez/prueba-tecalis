import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "../../environments/environment";

@Injectable()
export class RapidApiInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    const reqCopy = request.clone({
      headers: request.headers
        .set("x-rapidapi-key", environment["x-rapidapi-key"])
        .set("x-rapidapi-host", environment["x-rapidapi-host"]),
    });

    return next.handle(reqCopy);
  }
}
